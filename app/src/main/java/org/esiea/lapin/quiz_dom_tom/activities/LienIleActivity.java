package org.esiea.lapin.quiz_dom_tom.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import org.esiea.lapin.quiz_dom_tom.R;

public class LienIleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lien_ile);

        //Bouton guadeloup
        ImageButton button_guadeloupe = (ImageButton) findViewById(R.id.button_guadeloupe);
        button_guadeloupe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://www.regionguadeloupe.fr/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        //Bouton martinique
        ImageButton button_martinique = (ImageButton) findViewById(R.id.button_martinique);
        button_martinique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://www.collectivitedemartinique.mq/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        //Bouton reunion
        ImageButton button_reunion = (ImageButton) findViewById(R.id.button_reunion);
        button_reunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://www.regionreunion.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        //Bouton guyane
        ImageButton button_guyane = (ImageButton) findViewById(R.id.button_guyane);
        button_guyane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.ctguyane.fr/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
}
