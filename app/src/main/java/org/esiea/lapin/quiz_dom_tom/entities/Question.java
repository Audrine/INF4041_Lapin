package org.esiea.lapin.quiz_dom_tom.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Audrine on 14/12/16.
 */
@DatabaseTable(tableName = "questions")
public class Question implements Parcelable{
    @DatabaseField(generatedId = true)
    private int id ;
    @DatabaseField(columnName = "theme")
    private String theme = new String();
    @DatabaseField(columnName = "la_question")
    private String la_question = new String();
    @DatabaseField(columnName = "reponse")
    private int reponse;
    @DatabaseField(columnName = "detail_reponse")
    private String detail_reponse = new String();

    public Question (){}
    public Question(int id, String theme, String la_question, String detail_question, int reponse) {
        this.id = id;
        this.theme = theme;
        this.la_question = la_question;
        this.reponse = reponse;
        this.detail_reponse = detail_question;
    }
    public Question(Parcel in) {
        this.id = in.readInt();
        this.theme = in.readString();
        this.la_question = in.readString();
        this.detail_reponse = in.readString();
        this.reponse = in.readInt();
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>()
    {
        @Override
        public Question createFromParcel(Parcel source)
        {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size)
        {
            return new Question[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getLa_question() {
        return la_question;
    }

    public void setLa_question(String la_question) {
        this.la_question = la_question;
    }

    public int getReponse() {
        return reponse;
    }

    public void setReponse(int reponse) {
        this.reponse = reponse;
    }

    public String getDetail_reponse() {
        return detail_reponse;
    }

    public void setDetail_reponse(String detail_reponse) {
        this.detail_reponse = detail_reponse;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(theme);
        dest.writeString(la_question);
        dest.writeString(detail_reponse);
        dest.writeInt(reponse);
    }
}
