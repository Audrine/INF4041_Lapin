package org.esiea.lapin.quiz_dom_tom.domaine;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.esiea.lapin.quiz_dom_tom.data.JSONParser;
import org.esiea.lapin.quiz_dom_tom.data.QuestionData;
import org.esiea.lapin.quiz_dom_tom.entities.Question;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Audrine on 14/12/16.
 */

public class QuestionDomaine {

    private Dao<Question, Long> questionDao;

    public QuestionDomaine(Context quizdomtomContext) {
        QuestionData questionData = OpenHelperManager.getHelper(quizdomtomContext, QuestionData.class);
        try{
            questionDao = questionData.getDao();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public List<Question> getListQuestions(String theme) {
        try {
            return questionDao.queryBuilder().where().eq("theme", theme).query();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONArray getJson(){
        JSONParser jsonParser = new JSONParser();
        try{
            JSONObject jsonObject = jsonParser.execute().get();
            return jsonObject.getJSONArray("results");
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }


    public void updateData(){
        JSONArray result = getJson();

        for (int i = 0; i < result.length(); ++i){
            try{
                JSONObject question_json = result.getJSONObject(i);
                Question question = new Question(
                        question_json.getInt("id"),
                        question_json.getString("theme"),
                        question_json.getString("la_question"),
                        question_json.getString("detail_reponse"),
                        question_json.getInt("reponse")
                );
                int check = questionDao.queryBuilder().where( ).eq("id", question_json.getInt("id")).query().size();
                if(check == 0){
                    questionDao.create(question);
                    Log.i("test", "log de test");
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

}
