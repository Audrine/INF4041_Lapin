package org.esiea.lapin.quiz_dom_tom.activities;

import android.content.Intent;

import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.esiea.lapin.quiz_dom_tom.R;
import org.esiea.lapin.quiz_dom_tom.domaine.QuestionDomaine;
import org.esiea.lapin.quiz_dom_tom.entities.Question;


import java.io.Serializable;
import java.util.List;


public class ThemeActivity extends AppCompatActivity {
    private String HISTOIRE = "Histoire";
    private String CULTURE = "Culture";
    private String GEOGRAPHIE = "Geographie";
    private int COUNTER = 10;
    private int SCORE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        //Bouton jouer
        final Button histoire = (Button) findViewById(R.id.button_histoire);
        histoire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuestionDomaine questionDomaine = new QuestionDomaine(ThemeActivity.this);
                List<Question> questionList = questionDomaine.getListQuestions("histoire");
                if(questionList.size() < 10){
                    Toast.makeText(getApplicationContext(), "Veuillez mettre à jour vos question dans le menu principal", Toast.LENGTH_LONG).show();

                }else{
                    Intent intent = new Intent(ThemeActivity.this, JeuActivity.class);
                    intent.putExtra("Theme", HISTOIRE);
                    intent.putExtra("Counter", COUNTER);
                    intent.putExtra("listQuestion",(Serializable) questionList);
                    intent.putExtra("score", SCORE);
                    startActivity(intent);
                }

            }
        });

        final Button geographie = (Button) findViewById(R.id.button_geographie);
        geographie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuestionDomaine questionDomaine = new QuestionDomaine(ThemeActivity.this);
                List<Question> questionList = questionDomaine.getListQuestions("geographie");
                if(questionList.size() < 10){
                    Toast.makeText(getApplicationContext(), "Veuillez mettre à jour vos question dans le menu principal", Toast.LENGTH_LONG).show();

                }else {
                    Intent intent = new Intent(ThemeActivity.this, JeuActivity.class);
                    intent.putExtra("Theme", GEOGRAPHIE);
                    intent.putExtra("Counter", COUNTER);
                    intent.putExtra("score", SCORE);
                    intent.putExtra("listQuestion", (Serializable) questionList);
                    startActivity(intent);
                }
            }
        });

        final Button culture = (Button) findViewById(R.id.button_culture);
        culture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuestionDomaine questionDomaine = new QuestionDomaine(ThemeActivity.this);
                List<Question> questionList = questionDomaine.getListQuestions("culture");

                if(questionList.size() < 10){
                    Toast.makeText(getApplicationContext(), "Veuillez mettre à jour vos question dans le menu principal", Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(ThemeActivity.this, JeuActivity.class);
                    intent.putExtra("Theme", CULTURE);
                    intent.putExtra("Counter", COUNTER);
                    intent.putExtra("score", SCORE);
                    intent.putExtra("listQuestion", (Serializable) questionList);
                    startActivity(intent);
                }
            }
        });
    }
}
