package org.esiea.lapin.quiz_dom_tom.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import org.esiea.lapin.quiz_dom_tom.R;

public class RegleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regle);
        TextView les_regles = (TextView) findViewById(R.id.les_regles);
        les_regles.setMovementMethod(new ScrollingMovementMethod());
    }
}
