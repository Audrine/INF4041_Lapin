package org.esiea.lapin.quiz_dom_tom.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.esiea.lapin.quiz_dom_tom.R;

public class FinJeuActivity extends AppCompatActivity {
    private int score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent= getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            score = (int) bundle.get("score");
        }

        setContentView(R.layout.activity_fin_jeu);
        TextView text_note = (TextView) findViewById(R.id.text_note);
        text_note.setText(Integer.toString(score)+"/10");
        Button menu_principal = (Button) findViewById(R.id.button_retour);
        menu_principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FinJeuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


}
