package org.esiea.lapin.quiz_dom_tom.data;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.esiea.lapin.quiz_dom_tom.entities.Question;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Audrine on 14/12/16.
 */

public class QuestionData extends OrmLiteSqliteOpenHelper {

    private String DB_PATH = null;
    private static String DB_NAME = "quizdomtom";
    private SQLiteDatabase quizdomtomDB;
    private Context quizdomtomContext;
    private Dao<Question, Long> questionDao;

    public QuestionData(Context context) {
        super(context, DB_NAME, null, 1);
        this.quizdomtomContext = context;
        DB_PATH = "/data/data/"+ quizdomtomContext.getPackageName()+"/databases/";
        Log.e("Path 1", DB_PATH);
        try {
            createDataBase();
        }catch (IOException e){
            e.printStackTrace();
        }


    }

    public void createDataBase() throws IOException{
        boolean dbExist = checkDatabase();
        if(dbExist){
        }else{
            this.getReadableDatabase();
            try{
                copyDataBase();
            }catch (IOException e){
                throw new Error("Error copy database");
            }
        }
    }
    private boolean checkDatabase(){
        SQLiteDatabase checkDB = null;
        try
        {
            String path = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
        }catch (SQLiteException e){
        }
        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, Question.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, Question.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void openDataBase(){
        String dpPath = DB_PATH + DB_NAME;
        quizdomtomDB = SQLiteDatabase.openDatabase(dpPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close(){
        if(quizdomtomDB != null){
            quizdomtomDB.close();
        }
        super.close();
    }

    public void closeDataBase(){
        if(quizdomtomDB != null){
            quizdomtomDB.close();
        }
    }

    private void copyDataBase() throws IOException {
        InputStream inputStream = quizdomtomContext.getAssets().open(DB_NAME);
        String outFilename = DB_PATH + DB_NAME;
        OutputStream outputStream = new FileOutputStream(outFilename);
        byte[] buffer = new byte[1024];
        int length;

        while((length = inputStream.read(buffer)) > 0){
            outputStream.write(buffer, 0, length);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    public Dao<Question, Long> getDao() throws SQLException {
        if(questionDao == null) {
            questionDao = getDao(Question.class);
        }
        return questionDao;
    }


}
