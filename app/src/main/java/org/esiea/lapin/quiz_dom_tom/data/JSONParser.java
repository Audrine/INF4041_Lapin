package org.esiea.lapin.quiz_dom_tom.data;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Audrine on 19/12/16.
 */
public class JSONParser extends AsyncTask<String, Void, JSONObject> {

    static InputStream is = null;

    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }
    @Override
    protected JSONObject doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL("http://mon-projet-api.eu/api/questions");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()){
                Log.i("oui", "C'est bon");
            }
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp="";
            while((tmp=reader.readLine())!=null)
                json.append(tmp).append("\n");
            reader.close();

            JSONObject data = new JSONObject(json.toString());
            // This value will be 404 if the request was not
            // successful

            return data;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }
    protected void onPostExecute(String page)
    {
        //onPostExecute
    }
}