package org.esiea.lapin.quiz_dom_tom.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.graphics.Color;

import org.esiea.lapin.quiz_dom_tom.R;
import org.esiea.lapin.quiz_dom_tom.entities.Question;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JeuActivity extends AppCompatActivity {
    private String name_theme = new String();
    private String reponse = new String();
    private List<Question> questionList = null;
    private Random randomGenerator = new Random();
    private final static String BONNE_REPONSE = "Bonne réponse";
    private final static String MAUVAISE_REPONSE = "Mauvaise réponse";
    private int counter;
    private int score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        TextView text_la_question = (TextView)findViewById(R.id.la_question);

        Intent intent= getIntent();
        Bundle bundle = intent.getExtras();
        final TextView theme = (TextView) findViewById(R.id.mon_theme);
        final TextView text_reponse = (TextView) findViewById(R.id.reponse_question);
        final TextView text_detail_reponse = (TextView) findViewById(R.id.detail_reponse);
        final Button button_continuer = (Button) findViewById(R.id.button_continuer);
        text_detail_reponse.setVisibility(View.GONE);
        text_reponse.setVisibility(View.GONE);
        button_continuer.setVisibility(View.GONE);
        final Button vrai = (Button) findViewById(R.id.button_vrai);
        final Button faux = (Button) findViewById(R.id.button_faux);


        if(bundle != null){
            this.name_theme = (String) bundle.get("Theme");
            this.questionList=  (List<Question>) bundle.getSerializable("listQuestion");
            counter = (int) bundle.get("Counter");
            score = (int) bundle.get("score");
        }

        final int index = randomGenerator.nextInt(questionList.size());
        final Question question = questionList.get(index);
        theme.setText(this.name_theme);
        text_la_question.setText(question.getLa_question());
        vrai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Est ce vrai
                String reponse;
                if(question.getReponse() == 1){
                    reponse = BONNE_REPONSE;
                    text_reponse.setTextColor(Color.GREEN);
                    score += 1;
                }else {
                    reponse = MAUVAISE_REPONSE;
                    text_reponse.setTextColor(Color.RED);
                }

                //Text
                text_detail_reponse.setText(question.getDetail_reponse());
                text_reponse.setText(reponse);

                //Visibilité
                text_reponse.setVisibility(View.VISIBLE);
                text_detail_reponse.setVisibility(View.VISIBLE);
                button_continuer.setVisibility(View.VISIBLE);
                vrai.setVisibility(View.INVISIBLE);
                faux.setVisibility(View.INVISIBLE);

            }
        });

        faux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Est ce faux
                String reponse;
                if(question.getReponse() == 0){
                    reponse = BONNE_REPONSE;
                    text_reponse.setTextColor(Color.GREEN);
                    score += 1;
                }else {
                    reponse = MAUVAISE_REPONSE;
                    text_reponse.setTextColor(Color.RED);
                }

                //Text
                text_detail_reponse.setText(question.getDetail_reponse());
                text_reponse.setText(reponse);

                //Visibilité
                text_reponse.setVisibility(View.VISIBLE);
                text_detail_reponse.setVisibility(View.VISIBLE);
                button_continuer.setVisibility(View.VISIBLE);
                vrai.setVisibility(View.INVISIBLE);
                faux.setVisibility(View.INVISIBLE);
            }
        });
        button_continuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter -= 1;
                Intent intent;
                if(counter <= 0){
                    intent = new Intent(JeuActivity.this, FinJeuActivity.class);
                    intent.putExtra("score", score);
                }else{
                    intent = new Intent(JeuActivity.this, JeuActivity.class);
                    questionList.remove(index);
                    intent.putExtra("listQuestion", (Serializable) questionList);
                    intent.putExtra("Theme", name_theme);
                    intent.putExtra("Counter", counter);
                    intent.putExtra("score", score);
                }
                startActivity(intent);
            }
        });
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Fermer l'application")
                .setMessage("Voullez vous quitter l'application ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }

                })
                .setNegativeButton("Non", null)
                .show();
    }

}
